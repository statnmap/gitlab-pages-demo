_This is a demo repository for {GitLab-Pages-Deploy}:  https://github.com/statnmap/GitLab-Pages-Deploy_

This creates HTML outputs published on GitLab-Pages, with one sub-folder for each branch of the project that you decide to be worth it..

## Automatic generation of documents

Documents are generated automatically thanks to Continuous Integration and Continuous Deployment. The latest version of these documents, with one version per specific branch, is available here:
*<https://statnmap.gitlab.io/gitlab-pages-demo/>*

## gh-pages

CI creates a specific branch to be deployed: <https://gitlab.com/statnmap/gitlab-pages-demo/-/tree/gh-pages>

- All HTML reports are created by the CI using R or pandoc directly
- Once created, there are stored in branch named `gh-pages`, by sub-folders (as variable `SITE_BY_BRANCH: "TRUE"` is set in the "gitlab-ci.yml" file).
- A commit is created with name of the person who trigered the CI and the name of the branch modified
- There is a directory for all branches that you declare in `stage: prepare-deploy`
- Each time the branch `gh-pages` is updated, its content is deployed on GitLab Pages thanks to `stage: deploy`

## A report for each important branch

The report is compiled and generated automatically upon merging to the main branch (`main`). The latest version is available here: <https://statnmap.gitlab.io/gitlab-pages-demo/main>
This version can be printed as a PDF as is.

Each specific branch like `production` or `validation` shows a version of the report in that branch :

- For `production`: <https://statnmap.gitlab.io/gitlab-pages-demo/production>
- For `validation`: <https://statnmap.gitlab.io/gitlab-pages-demo/validation>
All versions of specific branches are visible in the index: <https://statnmap.gitlab.io/gitlab-pages-demo/index.html>


_To change the appearance of HTML output (colors, spaces, ...), it is necessary to modify the css resources of this project or create a dedicated package._

## An updated report in odt format and others

The report in "odt" format can be downloaded in its latest version here: <https://statnmap.gitlab.io/gitlab-pages-demo/main/report.odt>

_Note: For this format, you have the possibility to change the default template included in the 'pandoc/reference.odt'_

Other formats are created there:

- {pagedown}: https://statnmap.gitlab.io/gitlab-pages-demo/main/_pagedown.html
- PDF with LateX: https://statnmap.gitlab.io/gitlab-pages-demo/main/_main.pdf

## General writing instructions

CRs are written collaboratively on GitLab directly or from a local RStudio session, each in their own branch.  
Content revisions are made via "Merge Requests" to the main branch.

The report is compiled and generated automatically upon merge to the main branch. The latest version is available here: <https://statnmap.gitlab.io/gitlab-pages-demo/main> 
This version can be printed as a PDF as is.

A complete process is explored by following the guide made for DREALs: <https://rdes_dreal.gitlab.io/publication_guide/collaborer-git.html>.
It will then be adapted to your use.
We will be able to identify the points that require a dedicated training.

The principle: everyone develops in a branch that is reversed in *main*

- Temporary branches for the first editors
- A regularly updated *validation* branch for the first reviewer
- A regularly updated *production* branch for the chief editor

This way, you can continue to feed the *main* even if there is a revision process in progress.
