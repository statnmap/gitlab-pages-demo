# gitlab-pages-demo 0.0.2

* Update README accordingly
* Use gh-pages to publish: https://github.com/statnmap/GitLab-Pages-Deploy/blob/main/README.md

# gitlab-pages-demo 0.0.1

* Added a `NEWS.md` file to track changes to the package.
